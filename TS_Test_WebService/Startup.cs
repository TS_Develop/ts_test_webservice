﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TS_Test_WebService.Startup))]

namespace TS_Test_WebService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Console.WriteLine("Prueba 1 del commit");
            Console.WriteLine("Prueba 2 del commit");
        }

        public void HolaMundo()
        {
            Console.WriteLine("hola mundo");
        }

        public void SumarNumero()
        {
            int a = 5;
            int b = 4;

            Console.WriteLine("El resultado de la suma entre a y b es: " + a + b);

        }
    }
}
